package thedrake;

import java.io.PrintWriter;
import java.util.*;

public class BoardTroops implements JSONSerializable{
	private final PlayingSide playingSide;
	private final Map<BoardPos, TroopTile> troopMap;
	private final TilePos leaderPosition;
	private final int guards;

	public BoardTroops(PlayingSide playingSide) { 
		this.playingSide = playingSide;
		this.troopMap = Collections.emptyMap();
		this.leaderPosition = TilePos.OFF_BOARD;
		this.guards = 0;
	}
	
	public BoardTroops(
			PlayingSide playingSide,
			Map<BoardPos, TroopTile> troopMap,
			TilePos leaderPosition, 
			int guards) {
		this.playingSide = playingSide;
		this.troopMap = troopMap;
		this.leaderPosition = leaderPosition;
		this.guards = guards;
	}

	public Optional<TroopTile> at(TilePos pos) {
		return Optional.ofNullable(troopMap.get((BoardPos)pos));
	}
	
	public PlayingSide playingSide() {
		return playingSide;
	}
	
	public TilePos leaderPosition() {
		return leaderPosition;
	}

	public int guards() {
		return guards;
	}
	
	public boolean isLeaderPlaced() {
		return leaderPosition != TilePos.OFF_BOARD;
	}
	
	public boolean isPlacingGuards() {
		return isLeaderPlaced() && guards < 2;
	}	
	
	public Set<BoardPos> troopPositions() {
		return troopMap.keySet();
	}

	public BoardTroops placeTroop(Troop troop, BoardPos target){
		if(at(target).isPresent())
		{ throw new IllegalArgumentException(); }

		TilePos lp = leaderPosition;
		int g = guards;
		if(!isLeaderPlaced())
		{ lp = target; }
		else if(g<2)
		{ ++g; }

		TroopTile tile = new TroopTile(troop,playingSide, TroopFace.AVERS);
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.put(target, tile);

		return new BoardTroops(playingSide(),newTroops,lp, g);
	}
	
	public BoardTroops troopStep(BoardPos origin, BoardPos target) {
		if (!isLeaderPlaced() || isPlacingGuards()){
			throw new IllegalStateException();
		}

		if(at(origin).isEmpty() || at(target).isPresent()){
			throw new IllegalArgumentException();
		}

		TilePos lp = leaderPosition;
		if(leaderPosition.equalsTo(origin.i(),origin.j()))
		{ lp = target; }

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);

		newTroops.put(target, tile);
		return new BoardTroops(playingSide(),newTroops,lp, guards).troopFlip(target);
	}
	
	public BoardTroops troopFlip(BoardPos origin) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");			
		}
		
		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");			
		}
		
		if(at(origin).isEmpty())
			throw new IllegalArgumentException();
		
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);
		newTroops.put(origin, tile.flipped());

		return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
	}
	
	public BoardTroops removeTroop(BoardPos target) {
		if (!isLeaderPlaced() || isPlacingGuards()){
			throw new IllegalStateException();
		}

		if(at(target).isEmpty()){
			throw new IllegalArgumentException();
		}

		TilePos lp = leaderPosition;
		if(leaderPosition.equalsTo(target.i(),target.j())){
			lp = TilePos.OFF_BOARD;
		}

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.remove(target);

		return new BoardTroops(playingSide(), newTroops, lp, guards);
	}

	@Override
	public void toJSON(PrintWriter writer) {
		writer.printf("{%s","\"side\":");
		playingSide().toJSON(writer);
		writer.printf("%s",",\"leaderPosition\":");
		leaderPosition().toJSON(writer);
		writer.printf("%s%d",",\"guards\":", guards());
		writer.printf("%s",",\"troopMap\":{");


		List<BoardPos> sortedKeys=new ArrayList<BoardPos>(troopMap.keySet());
		sortedKeys.sort(new Comparator<BoardPos>() {
			@Override
			public int compare(BoardPos o1, BoardPos o2) {
				if (o1.i() == o2.i())
				{  return Integer.compare(o1.j(), o2.j());  }
				if (o1.i() < o2.i())
				{ return -1; }
				return 1;
			}
		});
		Iterator<BoardPos> it = sortedKeys.iterator();
		while (it.hasNext())
		{
			BoardPos key = it.next();
			key.toJSON(writer);
			writer.printf("%c", ':');
			troopMap.get(key).toJSON(writer);

			if (it.hasNext())
			{ writer.printf("%c", ','); }
		}

		writer.printf("}}");
	}
}
