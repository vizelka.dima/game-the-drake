package thedrake.ui;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import thedrake.TroopTile;

public class CapturedView extends VBox {
    public CapturedView(int width){
        setPadding(new Insets(10));
        setPrefWidth(width + 20);
        setSpacing(2);
    }

    public void update(TroopTile troopTile) {
        TroopImageSet imageSet = new TroopImageSet(troopTile.troop().name());
        Image image = imageSet.get(troopTile.side(), troopTile.face());
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(70);
        imageView.setFitHeight(70);
        getChildren().add(imageView);
    }
}
