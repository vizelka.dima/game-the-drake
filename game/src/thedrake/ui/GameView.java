package thedrake.ui;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import thedrake.*;
import thedrake.ui.fxml.AppStageContext;

public class GameView extends BorderPane implements TileViewContext {

    private BoardView boardView;

    private TileView selected;

    private StackView orangeStack;
    private StackView blueStack;

    private StackView selectedStack;

    private GameState gameState;

    private ValidMoves validMoves;

    private final CapturedView orangeCaptured = new CapturedView(100);
    private final CapturedView blueCaptured = new CapturedView(100);

    private final Border blueBorder = new Border(
            new BorderStroke(Color.web("#00bfff"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(13)));
    private final Border orangeBorder = new Border(
            new BorderStroke(Color.web("#ff7f50"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(13)));

    private final AppStageContext appStageContext;

    public GameView(GameState gameState, AppStageContext appStageContext) {
        this.appStageContext = appStageContext;

        this.gameState = gameState;
        this.validMoves = new ValidMoves(gameState);

        this.boardView = new BoardView(gameState,this);

        this.orangeStack = new StackView(gameState, PlayingSide.ORANGE,this, 100);
        this.blueStack = new StackView(gameState, PlayingSide.BLUE,this, 100);

        updateBorder();

        setCenter(boardView);
        setTop(orangeStack);
        setBottom(blueStack);

        setLeft(orangeCaptured);
        setRight(blueCaptured);
    }

    private void updateBorder() {
        if (gameState.sideOnTurn() == PlayingSide.BLUE)
            setBorder(blueBorder);
        else
            setBorder(orangeBorder);
    }

    @Override
    public void tileViewSelected(TileView tileView) {
        if (selected != null && selected != tileView)
            selected.unselect();

        if (selectedStack != null)
            selectedStack.unselect();

        selected = tileView;
        selectedStack = null;

        boardView.clearMoves();
        boardView.showMoves(validMoves.boardMoves(tileView.position()));
    }

    @Override
    public void executeMove(Move move) {
        if (selected != null) {
            updateCapturedView(move);

            selected.unselect();
            selected = null;
        }
        else
            updateStack();

        boardView.clearMoves();
        gameState = move.execute(gameState);
        validMoves = new ValidMoves(gameState);
        boardView.updateTiles(gameState);

        checkEndGame();

        updateBorder();
    }

    private void checkEndGame() {
        if (validMoves.allMoves().isEmpty())
            gameState = gameState.draw();

        if (gameState.result() == GameResult.VICTORY ||
            gameState.result() == GameResult.DRAW)
            appStageContext.endMenu(gameState);
    }

    private void updateStack() {
        if (selectedStack!=null)
            selectedStack.pop();
        selectedStack = null;
    }

    private void updateCapturedView(Move move) {
        Tile tile = gameState.tileAt(move.target());
        if(tile.hasTroop()){
            if (gameState.sideOnTurn()==PlayingSide.ORANGE)
                orangeCaptured.update((TroopTile)tile);
            else
                blueCaptured.update((TroopTile)tile);
        }
    }

    @Override
    public void stackViewSelected(StackView stackView) {
        if (selected != null)
            selected.unselect();

        if (selectedStack!=null && selectedStack!=stackView)
            selectedStack.unselect();

        selectedStack = stackView;
        selected = null;
        boardView.clearMoves();

        if (stackView.getPlayingSide() == gameState.sideOnTurn()){
            stackView.select();
            boardView.showMoves(validMoves.movesFromStack());
        }
    }
}