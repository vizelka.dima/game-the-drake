package thedrake.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import thedrake.*;
import thedrake.ui.fxml.AppStageContext;
import thedrake.ui.fxml.EndGameController;
import thedrake.ui.fxml.MenuController;

import java.io.IOException;

public class TheDrakeApp extends Application implements AppStageContext {
    private Stage stage;
    private Scene startMenu;
    private Scene endMenu;
    private EndGameController endGameController;

    public static void main(String[] args) {
        launch(args);
    }

    public void play() {
        GameView gameView = new GameView(createSampleGameState(), this);
        stage.setScene(new Scene(gameView));
        stage.setTitle("Hra");
        stage.show();
    }

    public void startMenu() {
        stage.setScene(startMenu);
        stage.setTitle("Hlavní Menu");
        stage.show();
    }

    public void endMenu(GameState endState) {
        String finalTitle;
        if(endState.result() == GameResult.DRAW)
            finalTitle = "REMÍZA";
        else {
            String WonPlayerName = endState.sideNotOnTurn() == PlayingSide.BLUE? "MODRÝ" : "ORANŽOVÝ";
            finalTitle = WonPlayerName +" HRÁČ VYHRÁL";
        }
        endGameController.setResult(finalTitle);

        stage.setScene(endMenu);
        stage.setTitle("Konec hry");
        stage.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        Image icon = new Image(getClass().getResourceAsStream("fxml/images/logo.png"));
        stage.getIcons().add(icon);

        InitStartMenu();
        InitEndMenu();

        startMenu();
    }

    private void InitStartMenu() throws IOException {
        FXMLLoader loaderMenu = new FXMLLoader(getClass().getResource("fxml/menu.fxml"));
        loaderMenu.setControllerFactory( type -> new MenuController(this));
        //only one controller in  menu.fxml, so its ok

        Parent rootMenu = loaderMenu.load();
        startMenu = new Scene(rootMenu);
    }

    private void InitEndMenu() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/endGame.fxml"));
        loader.setControllerFactory( type ->  new EndGameController(this));

        Parent root = loader.load();
        endGameController = loader.getController();

        endMenu = new Scene(root);
    }

    public static GameState createSampleGameState() {
        Board board = new Board(4);
        PositionFactory positionFactory = board.positionFactory();
        board = board.withTiles(new Board.TileAt(positionFactory.pos(1, 1), BoardTile.MOUNTAIN)
//                                new Board.TileAt(positionFactory.pos(2, 1), BoardTile.MOUNTAIN),
//                                new Board.TileAt(positionFactory.pos(3, 2), BoardTile.MOUNTAIN),
//                                new Board.TileAt(positionFactory.pos(1, 2), BoardTile.MOUNTAIN),
//                                new Board.TileAt(positionFactory.pos(2, 2), BoardTile.MOUNTAIN)

        );

        return new StandardDrakeSetup().startState(board);
//                .placeFromStack(positionFactory.pos(0, 0))
//                .placeFromStack(positionFactory.pos(3, 3))
//                .placeFromStack(positionFactory.pos(0, 1))
//                .placeFromStack(positionFactory.pos(3, 2))
//                .placeFromStack(positionFactory.pos(1, 0))
//                .placeFromStack(positionFactory.pos(2, 3));
    }

}