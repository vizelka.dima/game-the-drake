package thedrake.ui.fxml;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    @FXML
    private Button close;

    private final AppStageContext appStageContext;

    public MenuController(AppStageContext appStageContext) {
        this.appStageContext = appStageContext;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) { }

    public void onPlay(){ appStageContext.play(); }

    public void onClose(){
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }
}
