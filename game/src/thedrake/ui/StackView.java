package thedrake.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import thedrake.*;

public class StackView extends HBox {
    private final TileViewContext tileViewContext;
    private final PlayingSide playingSide;

    private Border selectBorder = new Border(
            new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3)));


    public StackView(GameState gameState, PlayingSide playingSide,TileViewContext tileViewContext, int height){

        for (Troop troop : gameState.army(playingSide).stack()){
            TroopImageSet imageSet = new TroopImageSet(troop.name());
            Image image = imageSet.get(playingSide, TroopFace.AVERS);

            BackgroundImage bgImage = new BackgroundImage(
                   image, null, BackgroundRepeat.NO_REPEAT, null, null);

            Pane pane = new Pane();
            pane.setPrefSize(100,100);
            pane.setBackground(new Background(bgImage));
            getChildren().add(pane);
        }

        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(10));
        setSpacing(5);
        setPrefHeight(height + 20);

        this.playingSide = playingSide;
        this.tileViewContext = tileViewContext;
        setOnMouseClicked(e -> onClick());
    }

    private void onClick(){
        if (!getChildren().isEmpty())
            tileViewContext.stackViewSelected(this);
    }

    public PlayingSide getPlayingSide() {
        return playingSide;
    }

    public void pop() {
        if (!getChildren().isEmpty())
            getChildren().remove(0);
    }

    public void select() {
        Pane pane = (Pane) getChildren().get(0);
        pane.setBorder(selectBorder);
    }

    public void unselect(){
        Pane pane = (Pane) getChildren().get(0);
        pane.setBorder(null);
    }
}

