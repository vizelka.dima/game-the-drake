package thedrake;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile, JSONSerializable{
    Troop troop;
    PlayingSide side;
    TroopFace face;

    // Konstruktor
    public TroopTile(Troop troop, PlayingSide side, TroopFace face){
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    // Vrací barvu, za kterou hraje jednotka na této dlaždici
    public PlayingSide side(){
        return side;
    }

    // Vrací stranu, na kterou je jednotka otočena
    public TroopFace face(){
        return face;
    }

    // Jednotka, která stojí na této dlaždici
    public Troop troop(){
        return troop;
    }

    // Vrací False, protože na dlaždici s jednotkou se nedá vstoupit
    @Override
    public boolean canStepOn() {
        return false;
    }

    // Vrací True
    @Override
    public boolean hasTroop() {
        return true;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> res = new ArrayList<>();
        for ( TroopAction action : troop.actions(face)) {
            res.addAll(action.movesFrom(pos, side, state));
        }
        return res;
    }

    // Vytvoří novou dlaždici, s jednotkou otočenou na opačnou stranu
// (z rubu na líc nebo z líce na rub)
    public TroopTile flipped(){
        TroopFace newFace = (face == TroopFace.AVERS) ? TroopFace.REVERS : TroopFace.AVERS;
        return new TroopTile(troop, side, newFace);
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.printf("{\"%s\":","troop");
        troop().toJSON(writer);
        writer.printf(",\"%s\":","side");
        side().toJSON(writer);
        writer.printf(",\"%s\":","face");
        face().toJSON(writer);
        writer.printf("%c", '}');
    }
}
